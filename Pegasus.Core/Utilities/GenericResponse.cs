﻿using System;
namespace Pegasus.Core.Utilities
{
    public class GenericResponse<T> : Message
    {
        public T Response { get; set; }
    }

    public class Message
    {
        public string ErrorMessage { get; set; }
    }
}
