﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Pegasus.Core.Helpers;
using Pegasus.DAL.Models;
using Pegasus.DAL.Repositories;
using Pegasus.WebAPI.Models;
using Pegasus.WebAPI.Utilities;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Pegasus.WebAPI.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private IConfiguration _configuration;

        public UserController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        private string TokenFromHeader()
        {
            string token = "";
            string header = Request.Headers["Authorization"];
            var splittedToken = header.Split(" ");
            if(splittedToken != null)
            {
                token = splittedToken[1].ToString();
            }
            return token;
        }

        UserRepository _userRepository = new UserRepository();

        /* Post Requests */

        [AllowAnonymous]
        [HttpPost("login")]
        public IActionResult LoginUser([FromBody]Login login)
        {
            if(ModelState.IsValid)
            {
                if (login != null)
                {
                    var user = _userRepository.CheckLogin(login);
                    if (user.UserID > 0)
                    {
                        var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Token:SecretKey"]));
                        var credentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha512);
                        var claims = new[] {
                            new Claim(JwtRegisteredClaimNames.UniqueName, user.Username), // ClaimType.Name
                            new Claim(JwtRegisteredClaimNames.Email, user.EmailAddress), // ClaimType.EmailAddress
                            new Claim(JwtRegisteredClaimNames.NameId, user.UserID.ToString()), // ClaimType.NameIdentifier
                            new Claim(JwtRegisteredClaimNames.Jti, user.LoginKey), // jti
                            new Claim(JwtRegisteredClaimNames.Actort, user.UserKey), // ClaimType.Actor
                            new Claim(JwtRegisteredClaimNames.Birthdate, _userRepository.GetUserBirthDate(user.UserID).ToString()), // ClaimType.Birthdate
                            new Claim(ClaimTypes.Role, user.Type) // ClaimType.Role add role for authorization.
                        };

                        var stringToken = new JwtSecurityToken(
                            _configuration["Token:Issuer"],
                            _configuration["Token:Issuer"],
                            claims,
                            expires: DateTime.UtcNow.AddMinutes(Convert.ToInt32(_configuration["Token:ExpirationMinutes"])),
                            notBefore: DateTime.UtcNow,
                            signingCredentials: credentials
                        );

                        var userToken = new JwtSecurityTokenHandler().WriteToken(stringToken);
                        return Ok(new { token = userToken, loginKey = user.LoginKey });
                    }

                    if (user.IsBanned)
                        return Forbid("Your account has been banned.");
                    else if (user.IsDeleted)
                        return Forbid("Your account has been deleted.  Visit our nearest local site to retrieve your account.");
                    else if (user.IsDisabled)
                        return Forbid("Your account has been disabled.  To retrieve your account, enter the email address associated with your account.");
                    else
                        return NotFound();
                }
            } 
            else 
            {
                return BadRequest(ModelState);
            }
            return BadRequest();
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public IActionResult RegisterNewUser([FromBody]User userLogin)
        {
            if(ModelState.IsValid)
            {
                if(userLogin != null)
                {
                    bool result = _userRepository.RegisterNewUser(userLogin);
                    return Ok(new { isRegistered = result });
                }
            }
            else 
            {
                return BadRequest(ModelState);
            }
            return BadRequest();
        }

        [Authorize]
        [HttpPost("profile/save")]
        public IActionResult SaveUserProfile([FromBody] UserProfile userProfile)
        {
            if (ModelState.IsValid)
            {
                if (userProfile != null)
                {
                    if(_userRepository.CheckTokenIsNotBlacklisted(TokenFromHeader()))
                    {
                        var userID = Convert.ToInt32(HttpContext.User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(c => c.Value).FirstOrDefault());
                        var isUserProfileCreated = _userRepository.CreateUserProfile(userProfile, userID);
                        return Ok(new { isUserProfileCreated });
                    }
                    return Unauthorized();   
                }
            }
            else
            {
                return BadRequest(ModelState);    
            }
            return BadRequest();
        }

        /* Get Requests */

        [Authorize]
        [HttpGet("details")]
        public IActionResult GetUserDetails()
        {
            if (ModelState.IsValid)
            {
                if(_userRepository.CheckTokenIsNotBlacklisted(TokenFromHeader()))
                {
                    var userID = Convert.ToInt32(HttpContext.User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(c => c.Value).FirstOrDefault());
                    var userDetails = _userRepository.GetUserDetails(userID);
                    return Ok(userDetails);
                }
                return Unauthorized();
            }
            return BadRequest();
        }

        [Authorize]
        [HttpGet("profile/get")]
        public IActionResult GetUserProfile()
        {
            if(ModelState.IsValid)
            {
                if(_userRepository.CheckTokenIsNotBlacklisted(TokenFromHeader()))
                {
                    var userID = Convert.ToInt32(HttpContext.User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(c => c.Value).FirstOrDefault());
                    var userProfile = _userRepository.GetUserProfile(userID);
                    if (userProfile.FirstName != null && userProfile.LastName != null)
                    {
                        return Ok(userProfile);
                    }
                    else
                    {
                        return NotFound();
                    }
                }
                return Unauthorized();

            }
            return BadRequest();
        }

        /* Put Requests */

        [Authorize]
        [HttpPut("password/update")]
        public IActionResult UpdatePassword([FromBody] UserPassword userPassword)
        {
            if(ModelState.IsValid)
            {
                if(userPassword != null)
                {
                    if(_userRepository.CheckTokenIsNotBlacklisted(TokenFromHeader()))
                    {
                        var userID = Convert.ToInt32(HttpContext.User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(c => c.Value).FirstOrDefault());
                        var result = _userRepository.UpdateUserPassword(userPassword.OldPassword, userPassword.NewPassword, userID);
                        return Ok(new { IsPasswordUpdated = result });
                    }
                    return Unauthorized();
                }
            }
            else
            {
                return BadRequest(ModelState);
            }
            return BadRequest();
        }

        [Authorize]
        [HttpPut("profile/update")]
        public IActionResult UpdateUserProfile([FromBody] UserProfile userProfile)
        {
            if(ModelState.IsValid)
            {
                if (userProfile != null)
                {
                    if(_userRepository.CheckTokenIsNotBlacklisted(TokenFromHeader()))
                    {
                        var userID = Convert.ToInt32(HttpContext.User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(c => c.Value).FirstOrDefault());
                        var result = _userRepository.UpdateUserProfile(userProfile, userID);
                        return Ok(new { IsUserProfileUpdated = result });
                    }
                    return Unauthorized();
                }
            }
            else
            {
                return BadRequest(ModelState);
            }
            return BadRequest();
        }

        [AllowAnonymous]
        [HttpPost("token/refresh/{userkey}")]
        public IActionResult TokenRefresh(string userkey)
        {
            var user = new User();
            if(userkey.Length == 36)
            {
                user = _userRepository.CreateNewLoginKey(userkey);
                if(user.LoginKey != userkey)
                {
                    if(user.UserID > 0)
                    {
                        var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Token:SecretKey"]));
                        var credentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha512);
                        var claims = new[] {
                            new Claim(JwtRegisteredClaimNames.UniqueName, user.Username), // ClaimType.Name
                            new Claim(JwtRegisteredClaimNames.Email, user.EmailAddress), // ClaimType.EmailAddress
                            new Claim(JwtRegisteredClaimNames.NameId, user.UserID.ToString()), // ClaimType.NameIdentifier
                            new Claim(JwtRegisteredClaimNames.Jti, user.LoginKey), // jti
                            new Claim(JwtRegisteredClaimNames.Actort, user.UserKey), // ClaimType.Actor
                            new Claim(JwtRegisteredClaimNames.Birthdate, _userRepository.GetUserBirthDate(user.UserID).ToString()), // ClaimType.Birthdate
                            new Claim(ClaimTypes.Role, user.Type) // ClaimType.Role add role for authorization.
                        };

						// TODO: check user credential and update user token (this will create another token for user to continue using the web service)                  
                        var stringToken = new JwtSecurityToken(
                            _configuration["Token:Issuer"],
                            _configuration["Token:Issuer"],
                            claims,
							expires: DateTime.UtcNow.AddMinutes(Convert.ToInt32(_configuration["Token:ExpirationMinutes"])),
                            notBefore: DateTime.UtcNow,
                            signingCredentials: credentials
                        );



                        var userToken = new JwtSecurityTokenHandler().WriteToken(stringToken);
                        return Ok(new { token = userToken, loginKey = user.LoginKey });
                    }
                    else
                    {
                        return BadRequest();
                    }
                }
            }
            return BadRequest();
        }

        [Authorize]
        [HttpPost("logout/{userkey}")]
        public IActionResult UserLogout(string userkey)
        {
            var userID = Convert.ToInt32(HttpContext.User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(c => c.Value).FirstOrDefault());
            if(userID > 0)
            {
                var tokenBlacklist = new UserTokenBlacklist
                {
                    Token = TokenFromHeader(),
                    UserID = userID,
                    LoginKey = userkey

                };

                if (userkey.Length == 36 && TokenFromHeader() != null)
                {
                    var insertBlacklistToken = _userRepository.InsertTokenToBlacklist(tokenBlacklist);
                    if(insertBlacklistToken)
                    {
                        return Ok(new { isUserLogout = insertBlacklistToken });
                    }
                }
            }
            return BadRequest();
        }

        /* Delete Requests */



        /// <summary>
        /// MISC Functions Mainly for checking credentials
        /// </summary>
        /// <returns>The test.</returns>
        /* Checking claims. 
        [Authorize(Roles = "User")]
        [HttpGet("test")]
        public IActionResult Test()
        {
            var x = HttpContext.User.Claims.Select(c => new { Type = c.Type, Value = c.Value });
            var getUsername = HttpContext.User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).FirstOrDefault();
            var getExpiration = HttpContext.User.Claims.Where(c => c.Type == "exp").Select(c => c.Value).FirstOrDefault();
            var formatedTime = StringHelper.UnixTimeStampToDateTime(Convert.ToInt64(getExpiration));
            var getLoginKey = HttpContext.User.Claims.Where(c => c.Type == "jti").Select(c => c.Value).FirstOrDefault();
            return Ok(new { username = getUsername, claims = x, exp = formatedTime.ToString("yyyy-MM-dd hh:mm:ss tt"), token = TokenFromHeader(), jti = getLoginKey});
        }
        */
    }
}
