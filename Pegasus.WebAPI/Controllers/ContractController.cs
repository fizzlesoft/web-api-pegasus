﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Pegasus.DAL.Repositories;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Pegasus.WebAPI.Controllers
{
    [Route("api/[controller]")]
    public class ContractController : Controller
    {
        ContractRepository _contractRepository = new ContractRepository();

        [HttpGet("details/exclusive/region")]
        [Authorize(Roles = "User")]
        public IActionResult ContractDetails(int? regionID)
        {
            var contractDetails = _contractRepository.GetContractDetails(regionID);
            if(contractDetails.Select(c => c.Name) != null)
            {
                return Ok(contractDetails);
            }
            return NotFound();
        }


        [AllowAnonymous]
        [HttpGet("details")]
        public IActionResult ContractDetails()
        {
            var contractDetails = _contractRepository.GetContractDetails(0);
            if (contractDetails.Select(c => c.Name) != null)
            {
                return Ok(contractDetails);
            }
            return NotFound();
        }
    }
}
