﻿using System;
using System.ComponentModel.DataAnnotations;
using Pegasus.Core.Commands;
using Pegasus.DAL.Models;
namespace Pegasus.WebAPI.Models
{
    public class UserPassword
    {
        string _oldPassword;
        string _newPassword;
        string _confirmPassword;
        [Required]
        [RegularExpression("^((?=.*[a-z])(?=.*[A-Z])(?=.*\\d)).+$", ErrorMessage = "Password must contains atleast 1 lowercase, 1 uppercase letter and 1 number.")]
        [StringLength(50, ErrorMessage = "New Password must contains atleast 8 characters", MinimumLength = 8)]
        [DataType(DataType.Password)]
        public string NewPassword 
        { 
            get { return _newPassword; }
            set
            {
                value = DbCommand.HashPasswordToMD5(value);
                _newPassword = value;
            }
        }

        [Required]
        [RegularExpression("^((?=.*[a-z])(?=.*[A-Z])(?=.*\\d)).+$", ErrorMessage = "Password must contains atleast 1 lowercase, 1 uppercase letter and 1 number.")]
        [StringLength(50, ErrorMessage = "Confirm Password must contains atleast 8 characters", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [Compare("NewPassword", ErrorMessage = "Password Mismatch.")]
        public string ConfirmPassword
        {
            get { return _confirmPassword; }
            set 
            {
                value = DbCommand.HashPasswordToMD5(value);
                _confirmPassword = value;
            }
        }

        [Required]
        [RegularExpression("^((?=.*[a-z])(?=.*[A-Z])(?=.*\\d)).+$", ErrorMessage = "Password must contains atleast 1 lowercase, 1 uppercase letter and 1 number.")]
        [StringLength(50, ErrorMessage = "Old Password must contains atleast 8 characters", MinimumLength = 8)]
        [DataType(DataType.Password)]
        public string OldPassword
        {
            get { return _oldPassword; }
            set
            {
                value = DbCommand.HashPasswordToMD5(value);
                _oldPassword = value;
            }
        }
    }
}
