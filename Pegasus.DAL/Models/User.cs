﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Pegasus.Core.Commands;

namespace Pegasus.DAL.Models
{
    public class Login
    {
        string _password;

        [Required]
        [StringLength(50, MinimumLength = 5, ErrorMessage = "Username field must contains at least 5 characters.")]
        public string Username { get; set; }


        [Required]
        [StringLength(50, MinimumLength = 8, ErrorMessage = "Password field must contains at least 8 characters.")]
        [DataType(DataType.Password)]
        public string Password 
        { 
            get { return _password; }
            set 
            {
                _password = DbCommand.HashPasswordToMD5(value);
            }
        }
    }

    public class User : Login
    {
        string _userType;

        [JsonIgnore]
        public int UserID { get; set; }

        [EmailAddress(ErrorMessage = "You have inputted an invalid email address.")]
        public string EmailAddress { get; set; }

        [DataType(DataType.PhoneNumber, ErrorMessage = "Invalid contact number.")]
        public string ContactNumber { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        [JsonIgnore]
        public string Type
        {
            get { return _userType; }
            set
            {
                switch (Convert.ToInt32(value))
                {
                    case 2:
                        value = UserType.Contractor.ToString();
                        break;
                    case 3:
                        value = UserType.Admin.ToString();
                        break;
                    case 4:
                        value = UserType.Manager.ToString();
                        break;
                    case 9:
                        value = UserType.Root.ToString();
                        break;
                    default:
                        value = UserType.User.ToString();
                        break;
                }
                _userType = value;
            }
        }

        [JsonIgnore]
        public string UserKey { get; set; }
        [JsonIgnore]
        public bool IsDisabled { get; set; } = false;
        [JsonIgnore]
        public bool IsBanned { get; set; } = false;
        [JsonIgnore]
        public bool IsDeleted { get; set; } = false;
        [JsonIgnore]
        public DateTime DateCreated { get; set; } = DateTime.UtcNow;
        [JsonIgnore]
        public string LoginKey { get; set; }
        [JsonIgnore]
        public DateTime ExpirationMinutes { get; set; }
    }

    public class UserDetails
    {
        public string Username { get; set; }
        public string Email { get; set; }

        string _userType;
        public string Type 
        {
            get { return _userType; }
            set 
            {
                switch(Convert.ToInt32(value))
                {
                    case 2:
                        value = UserType.Contractor.ToString();
                        break;
                    case 3:
                        value = UserType.Admin.ToString();
                        break;
                    case 4:
                        value = UserType.Manager.ToString();
                        break;
                    case 9:
                        value = UserType.Root.ToString();
                        break;
                    default:
                        value = UserType.User.ToString();
                        break;
                }
                _userType = value;
            }
        }

    }

    public class UserProfile 
    {
        int _age;
        string _gender;

        [Key]
        [Required]
        [JsonIgnore]
        public int ProfileID { get; set; }
        [Required]
        [StringLength(50, ErrorMessage = "Firstname field must contains atleast 50 characters")]
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        [Required]
        [StringLength(50, ErrorMessage = "Firstname field must contains atleast 50 characters")]
        public string LastName { get; set; }

        [Required]
        [Range(18, 150, ErrorMessage = "Age must be atleast 18 years old and above.")]
        public int Age
        {
            get { return _age; }
            set
            {
                if (value >= 18 && value < 150)
                {
                    _age = value;
                }
            }
        }

        [Required]
        public string Gender
        {
            get { return _gender; }
            set
            {
                switch(Convert.ToInt32(value))
                {
                    case 2:
                        GenderType.Female.ToString();
                        break;
                    case 3:
                        GenderType.Other.ToString();
                        break;
                    default:
                        GenderType.Male.ToString();
                        break;
                }

                _gender = value;
            }
        }

        [Required]
        public DateTime BirthDate { get; set; }
        [Required]
        public string Street { get; set; }
        [Required]
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string Province { get; set; }
        [Required]
        public int ZipCode { get; set; }
        [Required]
        public string Region { get; set; }   
    }

    public class UserLogin
    {
        public int UserLoginID { get; set; }
        public int UserID { get; set; }
        public bool IsActiveLogin { get; set; }
        public DateTime LoginTime { get; set; } = DateTime.UtcNow;
        public int LoginAttempts { get; set; } = 0;
        public DateTime DateCreated { get; set; } = DateTime.UtcNow;
        public string LoginKey { get; set; }
        public DateTime ExpirationMinutes { get; set; }
    }

    public class LoginAttempt
    {
        public int CountAttempts { get; set; }
        public bool IsDisabled { get; set; }
    }

    public enum UserType
    {
        User = 1,
        Contractor = 2,
        Admin = 3,
        Manager = 4,
        Root = 9
    }

    public enum GenderType
    {
        Male = 1,
        Female = 2,
        Other = 3
    }


    public class UserTokenBlacklist
    {
        public int UserID { get; set; }
        public string Token { get; set; }
        public string LoginKey { get; set; }
    }
}
