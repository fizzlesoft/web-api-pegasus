﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Pegasus.DAL.Models
{
    public class Contract
    {
        [Key]
        [JsonIgnore]
        public int ContractID { get; set; }
        [JsonIgnore]
        public int? RegionID { get; set; }
        [Required]
        [StringLength(50, ErrorMessage = "Name must contains not more than 50 characters.")]
        public string Name { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "Description must contains not more than 100 characters.")]
        public string Description { get; set; }
        [Required]
        [Range(0, 360, ErrorMessage = "Duration has a maximum duration is only 350 days.")]
        public int Duration { get; set; }
        [JsonIgnore]
        public string PromoCode { get; set; }
        [Required]
        [DataType(DataType.Currency)]
        public decimal ContractPrice { get; set; }
        public decimal Discount { get; set; }
        [DataType(DataType.Currency)]
        public decimal DiscountedPrice { get; set; }
    }
}
