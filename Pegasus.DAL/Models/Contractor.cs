﻿using System;

namespace Pegasus.DAL.Models
{
	public class UserContract
	{
		public string ContractKey { get; set; }
		public string SignKey { get; set; }
		public string PortKey { get; set; }
		public DateTime EffectivityDate { get; set; }
	}   
}
