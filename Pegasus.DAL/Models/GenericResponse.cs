﻿using System;

namespace Pegasus.WebAPI.Utilities
{
    public class GenericResponse<T> : Message
    {
        public T Reponse { get; set; }
    }

    public class Message
    {
        public string ErrorMessage { get; set; }
    }
}
