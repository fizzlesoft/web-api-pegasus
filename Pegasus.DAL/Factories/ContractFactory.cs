﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Pegasus.Core.Commands;
using Pegasus.Core.Helpers;
using Pegasus.DAL.Interfaces;
using Pegasus.DAL.Models;

namespace Pegasus.DAL.Factories
{
    public class ContractFactory : IContractFactory
    {
        public IList<Contract> GetContractDetails(int? regionID)
        {
            var contracts = new List<Contract>();
            var sqlQuery = @"
                SELECT 
                    c.[Name],
                    c.Description,
                    c.Duration,
                    c.ContractPrice,
                    c.Discount,
                    DiscountedPrice = (c.ContractPrice - (c.ContractPrice * (c.Discount / 100)))
                FROM Contract c
                LEFT OUTER JOIN Region r ON r.RegionID = c.RegionID
                WHERE c.RegionID IS NULL
                    OR c.RegionID = @RegionID";
            using (var rs = DbCommand.ExecuteReader(sqlQuery,
                                                    new SqlParameter("@RegionID", regionID != null ? regionID : 0)))
            {
                while (rs.Read())
                {
                    var contract = new Contract
                    {
                        Name = ConvertHelper.GetString(rs, 0),
                        Description = ConvertHelper.GetString(rs, 1),
                        Duration = ConvertHelper.GetInt32(rs, 2),
                        ContractPrice = ConvertHelper.GetDecimal(rs, 3),
                        Discount = ConvertHelper.GetDecimal(rs, 4),
                        DiscountedPrice = ConvertHelper.GetDecimal(rs, 5)
                    };

                    contracts.Add(contract);
                }
            }
            return contracts;
        }
    }
}
