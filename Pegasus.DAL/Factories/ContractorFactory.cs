﻿using System;
using System.Data.SqlClient;
using Pegasus.Core.Commands;
using Pegasus.Core.Helpers;
using Pegasus.DAL.Interfaces;
using Pegasus.DAL.Models;

namespace Pegasus.DAL.Factories
{
	public class ContractorFactory : IContractor
	{
		public string CreateContractorLink(int userID, int contractID)
		{
			string result = string.Empty;
			if(userID > 0)
			{
				var sqlQuery = @"
                SELECT
                    COUNT(1)
                FROM ContractSign cs
                WHERE UserID = @UserID";
				using(var rs = DbCommand.ExecuteReader(sqlQuery,
				                                       new SqlParameter("@UserID", userID)))
				{
					if(rs.Read())
					{
						if(ConvertHelper.GetInt32(rs, 0) == 0)
                        {
							var sqlQueryGetContractKey = @"
                            SELECT
                                RIGHT(ContractKey, 12)
                            FROM Contract
                            WHERE ContractID = @ContractID";
							using(var rsGetContractKey = DbCommand.ExecuteReader(sqlQueryGetContractKey,
							                                                     new SqlParameter("@ContractID", contractID)))
							{
								if(rs.Read())
								{
									if(ConvertHelper.GetString(rsGetContractKey, 0).Length == 12)
									{
										var sqlQueryGetUserKey = @"
                                        SELECT
                                            RIGHT(UserKey, 12)
                                        FROM [User]
                                        WHERE UserID = @UserID";
										using(var rsUserKey = DbCommand.ExecuteReader(sqlQueryGetUserKey,
										                                              new SqlParameter("@UserID", userID)))
										{
											if(rsUserKey.Read())
											{
												if(ConvertHelper.GetString(rsUserKey, 0).Length == 12)
												{
													var portKey = Guid.NewGuid();
													var sqlInsertContractSign = @"
                                                    INSERT INTO ContractSign(UserID, ContractKey, SignKey, PortKey, EffectivityDate)
                                                    VALUES(@UserID, @ContractKey, @SignKey, @PortKey, @EffectivityDate)";
                                                    var affectedRow = DbCommand.ExecuteNonQuery(sqlInsertContractSign,
                                                                                                new SqlParameter("@UserID", userID),
													                                            new SqlParameter("@ContractKey", ConvertHelper.GetString(rsGetContractKey, 0)),
													                                            new SqlParameter("@SignKey", ConvertHelper.GetString(rsUserKey, 0)),
													                                            new SqlParameter("@PortKey", portKey),
													                                            new SqlParameter("@EffectivityDate", DateTime.UtcNow.AddHours(2)));
													if(affectedRow > 0)
													{
														var sqlGetContractSignDetails = @"
                                                        SELECT TOP 1
                                                            cs.ContractKey,
                                                            cs.SignKey,
                                                            CONVERT(VARCHAR(25), RIGHT(cs.PortKey, 12))
                                                        FROM ContractSign cs
                                                        INNER JOIN [User] u ON c.UserID = cs.UserID
                                                        WHERE cs.UserID = @UserID
                                                            AND cs.PortKey = @PortKey
                                                        ORDER BY EffectivityDate DESC";
														using(var rsGetContractSign = DbCommand.ExecuteReader(sqlGetContractSignDetails,
														                                                      new SqlParameter("@UserID", userID),
														                                                      new SqlParameter("@PortKey",portKey)))
														{
															if(rsGetContractSign.Read())
															{
																result = ConvertHelper.GetString(rsGetContractSign, 0) + userID + ConvertHelper.GetString(rsGetContractSign, 1) + ConvertHelper.GetString(rsGetContractKey, 2);
															}
														}
													}
												}
											}
										}                              
									}
								}
							}
                        }
						else if(ConvertHelper.GetInt32(rs, 0) == 1)
						{
							var sqlQueryGetContractSign = @"
                            SELECT TOP 1
                                cs.ContractKey,
                                cs.SignKey,
                                CONVERT(VARCHAR(25), RIGHT(cs.PortKey, 12))
                            FROM [User] u
                            INNER JOIN ContractSign cs ON cs.UserID = u.UserID
                            INNER JOIN [Contract] c ON c.ContractID = RIGHT(c.ContractKey, 12)
                            WHERE cs.UserID = @UserID
                                AND c.ContractID = @ContractID";
							using(var rsQueryGetContractSign = DbCommand.ExecuteReader(sqlQueryGetContractSign,
							                                                           new SqlParameter("@UserID", userID),
							                                                           new SqlParameter("@ContractID", contractID)))
							{
								if(rsQueryGetContractSign.Read())
								{
									result = ConvertHelper.GetString(rsQueryGetContractSign, 0) + userID + ConvertHelper.GetString(rsQueryGetContractSign, 1) + ConvertHelper.GetString(rsQueryGetContractSign, 2);
								}
							}                     
						}
						else                     
						{
							int count = 0;
							var sqlQueryGetContractSign = @"
                            SELECT
                                cs.ContractKey,
                                cs.SignKey,
                                CONVERT(VARCHAR(25), RIGHT(cs.PortKey, 12)),
                                cs.ContractSignID
                            FROM [User] u
                            INNER JOIN ContractSign cs ON cs.UserID = u.UserID
                            INNER JOIN [Contract] c ON c.ContractID = RIGHT(c.ContractKey, 12)
                            WHERE cs.UserID = @UserID
                                AND c.ContractID = @ContractID
                            ORDER BY ContractSignID ASC";
                            using(var rsQueryGetContractSign = DbCommand.ExecuteReader(sqlQueryGetContractSign,
                                                                                       new SqlParameter("@UserID", userID),
                                                                                       new SqlParameter("@ContractID", contractID)))
                            {
								while(rsQueryGetContractSign.Read())
								{
									count++;
                                    if(count == 1)
									{
										result = ConvertHelper.GetString(rsQueryGetContractSign, 0) + userID + ConvertHelper.GetString(rsQueryGetContractSign, 1) + ConvertHelper.GetString(rsQueryGetContractSign, 2);                                                      
									}
									else
									{
										var sqlDelete = @"DELETE FROM ContractSign WHERE ContractSignID = @ContractSignID";
										var affectedRows = DbCommand.ExecuteNonQuery(sqlDelete,
										                                             new SqlParameter("@ContractSignID", ConvertHelper.GetInt32(rsQueryGetContractSign, 3)));
										if(affectedRows > 0)
										{
											return result;
										}
									}                           
								}
                            }         
						}
					}               
				}
			}
			return result;
		}
	}
}
