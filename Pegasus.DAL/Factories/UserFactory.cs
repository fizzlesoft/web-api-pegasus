﻿using System;
using System.Data.SqlClient;
using System.IO;
using Microsoft.Extensions.Configuration;
using Pegasus.Core.Commands;
using Pegasus.Core.Helpers;
using Pegasus.DAL.Interfaces;
using Pegasus.DAL.Models;

namespace Pegasus.DAL.Factories
{
    public class UserFactory : IUser
    {
        public static IConfiguration Configuration { get; set; }
        public static string GetJsonValue(string connectionString)
        {
            string result = "";
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            result = Configuration[connectionString];
            return result;
        }

        /// <summary>
        /// Get the important credentials in creating a token.
        /// </summary>
        /// <returns>The login credentials.</returns>
        /// <param name="userID">User identifier.</param>
        public User GetLoginCredentials(int userID)
        {
            var userLogin = new User();
            if(userID > 0)
            {
                var sqlQuery = @"
                SELECT 
                    u.Username, 
                    u.EmailAddress, 
                    u.ContactNumber, 
                    u.[Type], 
                    u.UserKey, 
                    u.DateCreated,
                    ul.LoginKey
                FROM [User] u
                INNER JOIN UserLogin ul ON ul.UserID = u.UserID
                WHERE u.UserID = @UserID";
                using(var rsUser = DbCommand.ExecuteReader(sqlQuery, new SqlParameter("@UserID", userID)))
                {
                    if(rsUser.Read())
                    {
                        userLogin = new User
                        {
                            UserID = userID,
                            Username = ConvertHelper.GetString(rsUser, 0),
                            EmailAddress = ConvertHelper.GetString(rsUser, 1),
                            ContactNumber = ConvertHelper.GetString(rsUser, 2),
                            Type = ConvertHelper.GetString(rsUser, 3),
                            UserKey = ConvertHelper.GetString(rsUser, 4),
                            DateCreated = ConvertHelper.GetDateTime(rsUser, 5),
                            LoginKey = ConvertHelper.GetString(rsUser, 6)
                        };
                    }
                }
            }
            return userLogin;
        }

        /// <summary>
        /// Checks the username and password and return a UserID.
        /// </summary>
        /// <returns>The username and password.</returns>
        /// <param name="username">Username.</param>
        /// <param name="password">Password.</param>
        public int CheckUsernameAndPassword(string username, string password)
        {
            int userID = 0;
            if (username != null && password != null)
            {
                var sqlQuery = @"
                        SELECT 
                            ul.UserID
                        FROM [User] ul 
                        WHERE ul.Password = @Password
                            AND ul.Username = @Username";
                using (var rsLogin = DbCommand.ExecuteReader(sqlQuery,
                                                             new SqlParameter("@Password", password),
                                                             new SqlParameter("@Username", username)))
                {
                    if (rsLogin.Read())
                    {
                        if(ConvertHelper.GetInt32(rsLogin, 0) != 0)
                        {
                            var sqlUpdateLoginAttempts = @"
                            UPDATE UserLogin
                            SET LoginAttempts = 0
                            WHERE UserID = @UserID";
                            DbCommand.ExecuteNonQuery(sqlUpdateLoginAttempts,
                                                      new SqlParameter("@UserID", ConvertHelper.GetInt32(rsLogin, 0)));
                            userID = ConvertHelper.GetInt32(rsLogin, 0);
                        }
                    }
                }
            }
            return userID;
        }

        /// <summary>
        /// Check if user already exist.
        /// </summary>
        /// <returns><c>true</c>, if user exist was checked, <c>false</c> otherwise.</returns>
        /// <param name="username">Username.</param>
        /// <param name="email">Email.</param>
        /// <param name="contactNumber">Contact number.</param>
        public bool CheckUserExist(string username, string email, string contactNumber)
        {
            bool result = false;
            var sqlQuery = @"
            SELECT
                COUNT(1)
            FROM [User]
            WHERE Username = @Username
                OR EmailAddress = @EmailAddress
                OR ContactNumber = @ContactNumber";
            result = DbCommand.ExecuteScalar<int>(sqlQuery,
                                                  new SqlParameter("@Username", username),
                                                  new SqlParameter("@EmailAddress", email),
                                                  new SqlParameter("@ContactNumber", contactNumber)) > 0 ? true : false;
            return result;
        }

        /// <summary>
        /// Register a new user.
        /// </summary>
        /// <returns><c>true</c>, if user was registered, <c>false</c> otherwise.</returns>
        /// <param name="userLogin">User login.</param>
        public bool RegisterUser(User userLogin)
        {
            bool result = false;

            if(userLogin != null)
            {
                if(string.IsNullOrEmpty(userLogin.UserKey)) 
                {
                    userLogin.UserKey = Guid.NewGuid().ToString();
                    if(userLogin.UserKey.Length == 36)
                    {
                        var sqlQuery = @"
                            INSERT INTO [User] (Username, Password, EmailAddress, ContactNumber, Type, UserKey, IsDisabled, IsDeleted, DateCreated)
                            VALUES(@Username, @Password, @EmailAddress, @ContactNumber, @Type, @UserKey, @IsDisabled, @IsDeleted, @DateCreated)";
                        var insertQuery = DbCommand.ExecuteNonQuery(sqlQuery,
                                                                    new SqlParameter("@Username", userLogin.Username),
                                                                    new SqlParameter("@Password", userLogin.Password),
                                                                    new SqlParameter("@EmailAddress", userLogin.EmailAddress),
                                                                    new SqlParameter("@ContactNumber", userLogin.ContactNumber),
                                                                    new SqlParameter("@Type", "1"),
                                                                    new SqlParameter("@UserKey", userLogin.UserKey),
                                                                    new SqlParameter("IsDisabled", userLogin.IsDisabled),
                                                                    new SqlParameter("@IsDeleted", userLogin.IsDeleted),
                                                                    new SqlParameter("@DateCreated", userLogin.DateCreated));
                        if(insertQuery > 0)
                        {

                            var sqlGetID = @"
                                         SELECT 
                                            UserID 
                                         FROM [User] 
                                         WHERE Username = @Username";
                            var getUserID = DbCommand.ExecuteScalar<int>(sqlGetID,
                                                                             new SqlParameter("@Username", userLogin.Username));
                            if(getUserID > 0) 
                            {
                                var sqlInsert = @"
                                            INSERT INTO UserProfile(UserID, FirstName, LastName)
                                            VALUES(@UserID, @FirstName, @LastName)";
                                var query = DbCommand.ExecuteNonQuery(sqlInsert,
                                                                      new SqlParameter("@UserID", getUserID),
                                                                      new SqlParameter("@FirstName", userLogin.FirstName),
                                                                      new SqlParameter("@LastName", userLogin.LastName));
                                if (query > 0)
                                {
                                    result = true;
                                }   
                            }
                        }
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Check the old password if it is already used.
        /// </summary>
        /// <returns><c>true</c>, if old password was checked, <c>false</c> otherwise.</returns>
        /// <param name="password">Password.</param>
        /// <param name="userID">User identifier.</param>
        bool CheckOldPassword(string password, int userID)
        {
            bool result = false;
            var sqlQuery = @"SELECT 1 FROM [User] WHERE Password = @Password AND UserID = @UserID";
            using(var rs = DbCommand.ExecuteReader(sqlQuery,
                                                   new SqlParameter("@Password", password),
                                                   new SqlParameter("@UserID", userID)))
            {
                if(rs.Read())
                {
                    result = true;
                }
            }
            return result;
        }

        /// <summary>
        /// Update user's password.
        /// </summary>
        /// <returns><c>true</c>, if new password was updated, <c>false</c> otherwise.</returns>
        /// <param name="newPassword">New password.</param>
        /// <param name="userID">User identifier.</param>
        public bool UpdateNewPassword(string newPassword, int userID)
        {
            bool result = false;
            if(userID > 0)
            {
                if(!CheckOldPassword(newPassword, userID))
                {
                    var sqlQuery = @"
                    UPDATE [User] 
                    SET Password = @Password 
                    WHERE UserID = @UserID";
                    var updateQuery = DbCommand.ExecuteNonQuery(sqlQuery,
                                                                new SqlParameter("@Password", newPassword),
                                                                new SqlParameter("@UserID", userID));
                    if (updateQuery > 0)
                    {
                        result = true;
                    }
                }
            }
            
            return result;
        }

        /// <summary>
        /// Gets the user details.
        /// </summary>
        /// <returns>The user details.</returns>
        /// <param name="userID">User identifier.</param>
        public UserDetails GetUserDetails(int userID)
        {
            var userDetails = new UserDetails();
            if(userID > 0)
            {
                var sqlQuery = @"
                    SELECT
                        Username,
                        EmailAddress,
                        [Type]
                    FROM [User]
                    WHERE UserID = @UserID";
                using (var rs = DbCommand.ExecuteReader(sqlQuery,
                                                        new SqlParameter("@UserID", userID)))
                {
                    if(rs.Read())
                    {
                        userDetails = new UserDetails
                        {
                            Username = ConvertHelper.GetString(rs, 0),
                            Email = ConvertHelper.GetString(rs, 1),
                            Type = ConvertHelper.GetString(rs, 2)
                        };
                    }
                }
            }
            return userDetails;
        }

        /// <summary>
        /// Create a user profile.
        /// </summary>
        /// <returns><c>true</c>, if user profile was created, <c>false</c> otherwise.</returns>
        /// <param name="userProfile">User profile.</param>
        /// <param name="userID">User identifier.</param>
        public bool CreateUserProfile(UserProfile userProfile, int userID)
        {
            bool result = false;
            if(userID > 0)
            {
                if (userProfile != null)
                {
                    var sqlInsertUserProfile = @"
                    INSERT INTO UserProfile (UserID, FirstName, MiddleName, LastName, Age, Gender, BirthDate, 
                                             Street, AddressLine1, AddressLine2, City, Province, ZipCode, Region)
                    VALUES(@UserID, @FirstName, @MiddleName, @LastName, @Age, @Gender, @BirthDate, 
                                             @Street, @AddressLine1, @AddressLine2, @City, @Province, @ZipCode, @Region)";
                    var insertUserProfile = DbCommand.ExecuteNonQuery(sqlInsertUserProfile,
                                                                      new SqlParameter("@UserID", userID),
                                                                      new SqlParameter("@FirstName", userProfile.FirstName),
                                                                      new SqlParameter("@MiddleName", userProfile.MiddleName),
                                                                      new SqlParameter("@LastName", userProfile.LastName),
                                                                      new SqlParameter("@Age", userProfile.Age),
                                                                      new SqlParameter("@Gender", userProfile.Gender),
                                                                      new SqlParameter("@BirthDate", userProfile.BirthDate),
                                                                      new SqlParameter("@Street", userProfile.Street),
                                                                      new SqlParameter("@AddressLine1", userProfile.AddressLine1),
                                                                      new SqlParameter("@AddressLine2", userProfile.AddressLine2),
                                                                      new SqlParameter("@City", userProfile.City),
                                                                      new SqlParameter("@Province", userProfile.Province),
                                                                      new SqlParameter("@ZipCode", userProfile.ZipCode),
                                                                      new SqlParameter("@Region", userProfile.Region));
                    if (insertUserProfile > 0)
                    {
                        result = true;
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Retrieve a user profile.
        /// </summary>
        /// <returns>The user profile.</returns>
        /// <param name="userID">User identifier.</param>
        public UserProfile GetUserProfile(int userID)
        {
            var userProfile = new UserProfile();

            if (userID > 0)
            {
                var sqlQueryGetProfile = @"
                SELECT
                    up.ProfileID,
                    up.FirstName,     
                    up.MiddleName,    
                    up.LastName,      
                    up.Age,           
                    up.Gender,        
                    up.BirthDate,     
                    up.Street,        
                    up.AddressLine1,  
                    up.AddressLine2,  
                    up.City,          
                    up.Province,      
                    up.ZipCode,       
                    up.Region       
                FROM UserProfile up
                INNER JOIN [User] ul ON ul.UserID = up.UserID
                WHERE ul.UserID = @UserID
                    --AND [Type] = '1'";
                using (var rs = DbCommand.ExecuteReader(sqlQueryGetProfile, new SqlParameter("@UserID", userID)))
                {
                    if (rs.Read())
                    {
                        userProfile = new UserProfile
                        {
                            ProfileID = ConvertHelper.GetInt32(rs, 0),
                            FirstName = ConvertHelper.GetString(rs, 1),
                            MiddleName = ConvertHelper.GetString(rs, 2),
                            LastName = ConvertHelper.GetString(rs, 3),
                            Age = ConvertHelper.GetInt32(rs, 4),
                            Gender = ConvertHelper.GetString(rs, 5),
                            BirthDate = ConvertHelper.GetDateTime(rs, 6),
                            Street = ConvertHelper.GetString(rs, 7),
                            AddressLine1 = ConvertHelper.GetString(rs, 8),
                            AddressLine2 = ConvertHelper.GetString(rs, 9),
                            City = ConvertHelper.GetString(rs, 10),
                            Province = ConvertHelper.GetString(rs, 11),
                            ZipCode = ConvertHelper.GetInt32(rs, 12),
                            Region = ConvertHelper.GetString(rs, 13),
                        };
                    }
                }
            }

            return userProfile;
        }

        /// <summary>
        /// Updates the user profile.
        /// </summary>
        /// <returns><c>true</c>, if user profile was updated, <c>false</c> otherwise.</returns>
        /// <param name="userProfile">User profile.</param>
        /// <param name="userID">User identifier.</param>
        public bool UpdateUserProfile(UserProfile userProfile, int userID)
        {
            bool result = false;
            if (userProfile != null)
            {
                var sqlUpdateProfile = @"
                    UPDATE UserProfile SET
                        FirstName = @FirstName,
                        MiddleName = @MiddleName,
                        LastName = @LastName,
                        Age = @Age,
                        Gender = @Gender,
                        BirthDate = @BirthDate,
                        Street = @Street,
                        AddressLine1 = @AddressLine1,
                        AddressLine2 = @AddressLine2,
                        City = @City,
                        Province = @Province,
                        ZipCode = @ZipCode,
                        Region = @Region
                    WHERE UserID = @UserID";
                var updateUserProfile = DbCommand.ExecuteNonQuery(sqlUpdateProfile,
                                                                  new SqlParameter("@UserID", userID),
                                                                  new SqlParameter("@FirstName", userProfile.FirstName),
                                                                  new SqlParameter("@MiddleName", userProfile.MiddleName),
                                                                  new SqlParameter("@LastName", userProfile.LastName),
                                                                  new SqlParameter("@Age", userProfile.Age),
                                                                  new SqlParameter("@Gender", userProfile.Gender),
                                                                  new SqlParameter("@BirthDate", userProfile.BirthDate),
                                                                  new SqlParameter("@Street", userProfile.Street),
                                                                  new SqlParameter("@AddressLine1", userProfile.AddressLine1),
                                                                  new SqlParameter("@AddressLine2", userProfile.AddressLine2),
                                                                  new SqlParameter("@City", userProfile.City),
                                                                  new SqlParameter("@Province", userProfile.Province),
                                                                  new SqlParameter("@ZipCode", userProfile.ZipCode),
                                                                  new SqlParameter("@Region", userProfile.Region));
                if (updateUserProfile > 0)
                {
                    result = true;
                }
            }
            return result;
        }

        /// <summary>
        /// Gets the user birth date.
        /// </summary>
        /// <returns>The user birth date.</returns>
        /// <param name="userID">User identifier.</param>
        public DateTime GetUserBirthDate(int userID)
        {
            var date = new DateTime();
            var sqlQuery = @"
                SELECT 
                    BirthDate 
                FROM UserProfile 
                WHERE UserID = @UserID";
            using(var rs = DbCommand.ExecuteReader(sqlQuery,
                                                   new SqlParameter("@UserID", userID)))
            {
                if(rs.Read())
                {
                    date = ConvertHelper.GetDateTime(rs, 0);
                }
            }
            return date;
        }

        /// <summary>
        /// Check if user is banned.
        /// </summary>
        /// <returns><c>true</c>, if user banned was ised, <c>false</c> otherwise.</returns>
        /// <param name="userID">User identifier.</param>
        public bool IsUserBanned(int userID)
        {
            bool result = false;
            if(userID > 0)
            {
                var sqlQuery = @"
                SELECT 
                    1
                FROM [User] ul
                INNER JOIN UserBanned ub ON ub.UserID = ul.UserID
                WHERE ul.UserID = @UserID
                    AND ul.IsDisabled = 1
                    AND DATEDIFF(DAY, GETDATE(), ub.BanDurationDate) > 0";
                result = DbCommand.ExecuteScalar<int>(sqlQuery, new SqlParameter("@UserID", userID)) > 0 ? true : false;
            }
            return result;
        }

        /// <summary>
        /// Check if user has active login.
        /// </summary>
        /// <returns><c>true</c>, if active login was ised, <c>false</c> otherwise.</returns>
        /// <param name="userID">User identifier.</param>
        public bool IsActiveLogin(int userID)
        {
            bool result = false;
            if(userID > 0)
            {
                var sqlQuery = @"
                SELECT
                    1
                FROM UserLogin ul
                INNER JOIN [User] u ON u.UserID = ul.UserID
                WHERE ul.UserID = @UserID
                    AND DATEDIFF(MINUTE, GETDATE(), ul.ExpirationMinutes) > 0";
                using(var rs = DbCommand.ExecuteReader(sqlQuery,
                                                       new SqlParameter("@UserID", userID)))
                {
                    if(rs.Read())
                    {
                        var isActiveLogin = ConvertHelper.GetInt32(rs, 0) > 0;
                        DbCommand.ExecuteNonQuery("UPDATE UserLogin SET IsActiveLogin = @IsActiveLogin WHERE UserID = @UserID",
                                                  new SqlParameter("@UserID", userID),
                                                  new SqlParameter("@IsActiveLogin", Convert.ToInt32(isActiveLogin)));
                        result = true;
                    }
                    else
                    {
                        var sqlDelete = @"
                        DELETE 
                        FROM UserLogin 
                        WHERE UserID = @UserID";
                        var rowsAffected = DbCommand.ExecuteNonQuery(sqlDelete, 
                                                                    new SqlParameter("@UserID",userID));
                        if(rowsAffected > 0)
                        {
                            result = false;
                        }
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Create a active login for a user.
        /// </summary>
        /// <returns><c>true</c>, if active login was inserted, <c>false</c> otherwise.</returns>
        /// <param name="userLogin">User login.</param>
        public bool InsertActiveLogin(UserLogin userLogin)
        {
            bool result = false;
            if(userLogin.UserLoginID == 0)
            {
                userLogin.IsActiveLogin = true;
                userLogin.LoginKey = Guid.NewGuid().ToString();
                var sqlInsert = @"
                INSERT INTO UserLogin (UserID, IsActiveLogin, LoginTime, LoginKey, LoginAttempts, DateCreated, ExpirationMinutes)
                VALUES(@UserID, @IsActiveLogin, @LoginTime, @LoginKey, @LoginAttempts, @DateCreated, @ExpirationMinutes)";
                var insertActiveLogin = DbCommand.ExecuteNonQuery(sqlInsert,
                                                                  new SqlParameter("@UserID", userLogin.UserID),
                                                                  new SqlParameter("@IsActiveLogin", userLogin.IsActiveLogin),
                                                                  new SqlParameter("@LoginTime", userLogin.LoginTime),
                                                                  new SqlParameter("@LoginAttempts", userLogin.LoginAttempts),
                                                                  new SqlParameter("@DateCreated", userLogin.DateCreated),
                                                                  new SqlParameter("@Loginkey", userLogin.LoginKey),
                                                                  new SqlParameter("@ExpirationMinutes", userLogin.ExpirationMinutes));
                result = insertActiveLogin > 0;
            }
            return result;
        }

        /// <summary>
        /// If user is failed to login using the username, it will identify if the user is disabled or not.
        /// </summary>
        /// <returns>The login attempt.</returns>
        /// <param name="username">Username.</param>
        public LoginAttempt FailLoginAttempt(string username)
        {
            var loginAttempt = new LoginAttempt();
            int attempts = 0;
            if(username.Length > 5 && !string.IsNullOrEmpty(username))
            {
                var sqlQueryGetUserID = @"
                SELECT
                    UserID
                FROM [User]
                WHERE Username = @Username";
                using (var rsUserID = DbCommand.ExecuteReader(sqlQueryGetUserID, 
                                                              new SqlParameter("@Username", username)))
                {
                    if(rsUserID.Read())
                    {
                        var userID = ConvertHelper.GetInt32(rsUserID, 0);
                        if(userID > 0)
                        {
                            var sqlQuery = @"
                            SELECT
                                LoginAttempts
                            FROM UserLogin ul
                            WHERE UserID = @UserID";
                            using (var rs = DbCommand.ExecuteReader(sqlQuery,
                                                                   new SqlParameter("@UserID", userID)))
                            {
                                if (rs.Read())
                                {
                                    attempts = ConvertHelper.GetInt32(rs, 0);
                                    if (attempts <= 5)
                                    {
                                        var sqlUpdate = @"
                                        UPDATE UserLogin
                                        SET LoginAttempts = (LoginAttempts + 1)
                                        WHERE UserID = @UserID";
                                        DbCommand.ExecuteNonQuery(sqlUpdate, 
                                                                  new SqlParameter("@UserID", userID));
                                        loginAttempt.CountAttempts = attempts + 1;
                                        loginAttempt.IsDisabled = false;
                                    }
                                    else
                                    {
                                        var sqlUpdate = @"
                                        UPDATE [User]
                                        SET IsDisabled = 1
                                        WHERE UserID = @UserID";
                                        DbCommand.ExecuteNonQuery(sqlUpdate, 
                                                                  new SqlParameter("@UserID", userID));
                                        loginAttempt.CountAttempts = attempts;
                                        loginAttempt.IsDisabled = true;
                                    }
                                }
                                else
                                {
                                    var userLogin = new UserLogin
                                    {
                                        UserID = userID,
                                        IsActiveLogin = false,
                                        LoginTime = DateTime.UtcNow,
                                        LoginAttempts = 1,
                                        DateCreated = DateTime.UtcNow
                                    };

                                    var insertAttempts = InsertActiveLogin(userLogin);
                                    if (insertAttempts)
                                    {
                                        loginAttempt.CountAttempts = 1;
                                        loginAttempt.IsDisabled = false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return loginAttempt;
        }

        /// <summary>
        /// Check if token is still valid or not.
        /// </summary>
        /// <returns><c>true</c>, if token valid was ised, <c>false</c> otherwise.</returns>
        /// <param name="loginKey">Login key.</param>
        public bool IsTokenValid(string loginKey)
        {
            bool result = false;
            if(loginKey.Length == 36)
            {
                var sqlQuery = @"
                SELECT
                    ul.UserID
                FROM UserLogin ul
                INNER JOIN [User] u ON u.UserID = ul.UserID
                WHERE LoginKey = @LoginKey";
                using(var rs = DbCommand.ExecuteReader(sqlQuery,
                                                       new SqlParameter("@LoginKey", loginKey)))
                {
                    if(rs.Read())
                    {
                        var userID = ConvertHelper.GetInt32(rs, 0);
                        if(userID != 0)
                        {
                            var sqlCheckValidToken = @"
                            SELECT
                                1
                            FROM UserLogin ul
                            WHERE DATEDIFF(MINUTE, GETDATE(), ul.ExpirationMinutes) > 0
                                AND UserID = @UserID";
                            bool isValidToken = DbCommand.ExecuteScalar<int>(sqlCheckValidToken,
                                                                             new SqlParameter("@UserID", userID)) > 0 ? true : false;
                            if(isValidToken)
                            {
                                result = true;
                            }
                        }
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Check if loginKey from that user is existed.
        /// </summary>
        /// <returns><c>true</c>, if login key exist was ised, <c>false</c> otherwise.</returns>
        /// <param name="loginKey">Login key.</param>
        public int GetUserIDByLoginkey(string loginKey)
        {
            int result = 0;
            if(loginKey.Length == 36)
            {
                var sqlQuery = @"
                SELECT
                    u.UserID
                FROM UserLogin ul
                INNER JOIN [User] u ON u.UserID = ul.UserID
                WHERE ul.LoginKey = @LoginKey";
                using(var rs = DbCommand.ExecuteReader(sqlQuery,
                                                       new SqlParameter("@LoginKey",loginKey)))
                {
                    if(rs.Read())
                    {
                        if(ConvertHelper.GetInt32(rs, 0) > 0)
                        {
                            result = ConvertHelper.GetInt32(rs, 0);
                        }
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Updates the user login.
        /// </summary>
        /// <returns>The user login.</returns>
        /// <param name="userID">User identifier.</param>
        /// <param name="expirationMinutes">Expiration minutes.</param>
        public string UpdateUserLogin(int userID, DateTime expirationMinutes)
        {
            var result = "";
            if(userID > 0)
            {
                var sqlQuery = @"
                SELECT
                    1
                FROM UserLogin ul
                INNER JOIN [User] u ON u.UserID = ul.UserID
                WHERE ul.UserID = @UserID
                    AND DATEDIFF(MINUTE, @DateTimeNow, ul.ExpirationMinutes) < 0";
                using(var rs = DbCommand.ExecuteReader(sqlQuery,
                                                       new SqlParameter("@UserID", userID),
                                                       new SqlParameter("@DateTimeNow", DateTime.UtcNow)))
                {
                    if(rs.Read())
                    {
                        if(ConvertHelper.GetInt32(rs, 0) != 0)
                        {
                            var sqlUpdate = @"
                                    UPDATE UserLogin
                                    SET LoginKey = LOWER(NEWID()),
                                        ExpirationMinutes = DATEADD(MINUTE, @TotalMinutes, @ExpirationMinute)
                                    WHERE UserID = @UserID";
                            var updated = DbCommand.ExecuteNonQuery(sqlUpdate,
                                                                    new SqlParameter("@UserID", userID),
                                                                    new SqlParameter("@ExpirationMinute", expirationMinutes),
                                                                    new SqlParameter("@TotalMinutes", Convert.ToInt32(GetJsonValue("Token:ExpirationMinutes"))));
                            if(updated > 0)
                            {
                                var sqlQuery2 = @"
                                SELECT TOP 1 
                                    LoginKey
                                FROM UserLogin ul
                                WHERE UserID = @UserID
                                ORDER BY UserLoginID DESC";
                                using(var rs2 = DbCommand.ExecuteReader(sqlQuery2, 
                                                                        new SqlParameter("@UserID", userID)))
                                {
                                    if(rs2.Read())
                                    {
                                        var newLoginKey = rs2[0].ToString();
                                        if(newLoginKey.Length == 36)
                                        {
                                            result = newLoginKey;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool UpdateUserToken(string loginKey)
		{
			bool result = false;
			if(loginKey.Length == 36)
			{
				var sqlQuery = @"
                SELECT
                    ul.UserID
                FROM UserLogin ul
                INNER JOIN [User] u ON u.UserID = ul.UserID
                WHERE LoginKey = @LoginKey
                    AND AND DATEDIFF(MINUTE, @DateTimeNow, ul.ExpirationMinutes) > 0";
				using(var rs = DbCommand.ExecuteReader(sqlQuery,
				                                       new SqlParameter("@LoginKey", loginKey),
				                                       new SqlParameter("@DateTimeNow", DateTime.UtcNow)))
				{
					if(rs.Read())
					{
						var userID = ConvertHelper.GetInt32(rs, 0);
						if(userID > 0)
						{
							var sqlUpdateQuery = @"
                            UPDATE UserLogin
                            SET ExpirationMinutes = @ExpirationMinutes
                            WHERE UserID = @UserID";
							result = DbCommand.ExecuteNonQuery(sqlUpdateQuery,
															   new SqlParameter("@ExpirationMinutes", DateTime.UtcNow),
							                                   new SqlParameter("@UserID", userID)) > 0;
						}
					}               
				}
			}         
			return result;
		}


        public bool CheckUserTokenIsBlacklisted(string token) 
        {
            bool result = false;
            if(token != null) 
            {
                var sqlCheckTokenExist = @"
                SELECT TOP 1
                    UserID
                FROM UserTokenBlacklist
                WHERE Token = @Token";
                using (var rs = DbCommand.ExecuteReader(sqlCheckTokenExist,
                                                       new SqlParameter("@Token", token)))
                {
                    if(rs.Read())
                    {
                        var userID = ConvertHelper.GetInt32(rs, 0);
                        if(userID > 0)
                        {
                            result = true;
                        }
                    }
                }
            }
            return result;
        }

        public bool InsertTokenBlacklist(UserTokenBlacklist tokenBlacklist)
        {
            bool result = false;
            if (tokenBlacklist != null)
            {
                var SqlInsertTokenBlacklist = @"
                INSERT INTO UsertokenBlacklist(UserID, Token, LoginKey, DateCreated)
                VALUES(@UserID, @Token, @LoginKey, GETDATE())";
                var numRowsAffected = DbCommand.ExecuteNonQuery(SqlInsertTokenBlacklist,
                                                                new SqlParameter("@UserID", tokenBlacklist.UserID),
                                                                new SqlParameter("@Token", tokenBlacklist.Token),
                                                                new SqlParameter("@LoginKey", tokenBlacklist.LoginKey));
                if (numRowsAffected > 0)
                {
                    result = true;
                }
            }
            return result;
        }
    }
}
