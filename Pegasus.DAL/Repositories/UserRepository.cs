﻿using System;
using Pegasus.DAL.Models;
using Pegasus.DAL.Factories;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace Pegasus.DAL.Repositories
{
    public class UserRepository
    {
        UserFactory _userFactory = new UserFactory();
        public static IConfiguration Configuration { get; set; }

        public string GetJsonValue(string connectionString)
        {
            string result = "";
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            result = Configuration[connectionString];
            return result;
        }

        /// <summary>
        /// Check if user has correct credentials.
        /// </summary>
        /// <returns>The login.</returns>
        /// <param name="login">Login.</param>
        public User CheckLogin(Login login)
        {
            var user = new User();
            if(login != null)
            {
                // user.ExpirationMinutes = DateTime.UtcNow.AddMinutes(Convert.ToInt32(GetJsonValue("Token:ExpirationMinutes")));
                user.ExpirationMinutes = DateTime.UtcNow.AddMinutes(2); // for testing!
                var userID = _userFactory.CheckUsernameAndPassword(login.Username, login.Password);        
                if(userID > 0)
                {
                    var checkUserBanned = _userFactory.IsUserBanned(userID);
                    if(!checkUserBanned)
                    {
                        var isActive = _userFactory.IsActiveLogin(userID);
                        if(isActive)
                        {
                            user = _userFactory.GetLoginCredentials(userID); 
                        }
                        else
                        {
                            var userLogin = new UserLogin();
                            userLogin.UserID = userID;
                            userLogin.ExpirationMinutes = user.ExpirationMinutes;
                            var insertLoginAttempt = _userFactory.InsertActiveLogin(userLogin);
                            if(insertLoginAttempt)
                            {
                                user = _userFactory.GetLoginCredentials(userID);
                            }
                        }
                    }
                    else
                    {
                        user.IsBanned = true;
                    }
                }
                else
                {
                    var failAttempt = _userFactory.FailLoginAttempt(login.Username);
                    if(failAttempt.IsDisabled)
                    {
                        user.IsDisabled = true;
                    }
                }
            }
            return user;
        }

        /// <summary>
        /// User registration.
        /// </summary>
        /// <returns><c>true</c>, if new user was registered, <c>false</c> otherwise.</returns>
        /// <param name="user">User.</param>
        public bool RegisterNewUser(User user)
        {
            var result = false;
            if(user != null)
            {
                var checkUserExist = _userFactory.CheckUserExist(user.Username, user.EmailAddress, user.ContactNumber);
                if(!checkUserExist)
                {
                    result = _userFactory.RegisterUser(user);
                }
            }

            return result;
        }

        /// <summary>
        /// Update user's password.
        /// </summary>
        /// <returns><c>true</c>, if user password was updated, <c>false</c> otherwise.</returns>
        /// <param name="oldPassword">Old password.</param>
        /// <param name="newPassword">New password.</param>
        /// <param name="userID">User identifier.</param>
        public bool UpdateUserPassword(string oldPassword, string newPassword, int userID)
        {
            bool result = false;
            if(oldPassword != newPassword)
            {
                if(userID > 0)
                {
                    result = _userFactory.UpdateNewPassword(newPassword, userID);   
                }
            }
            return result;
        }

        /// <summary>
        /// Gets the user details.
        /// </summary>
        /// <returns>The user details.</returns>
        /// <param name="userID">User identifier.</param>
        public UserDetails GetUserDetails(int userID)
        {
            var userDetails = new UserDetails();
            userDetails = _userFactory.GetUserDetails(userID);
            return userDetails;
        }

        /// <summary>
        /// Creates the user profile.
        /// </summary>
        /// <returns><c>true</c>, if user profile was created, <c>false</c> otherwise.</returns>
        /// <param name="userProfile">User profile.</param>
        /// <param name="userID">User identifier.</param>
        public bool CreateUserProfile(UserProfile userProfile, int userID)
        {
            bool result = _userFactory.CreateUserProfile(userProfile, userID);
            return result;
        }

        /// <summary>
        /// Gets the user profile.
        /// </summary>
        /// <returns>The user profile.</returns>
        /// <param name="userID">User identifier.</param>
        public UserProfile GetUserProfile(int userID)
        {
            var userProfile = new UserProfile();
            userProfile = _userFactory.GetUserProfile(userID);
            return userProfile;
        }

        /// <summary>
        /// Updates the user profile.
        /// </summary>
        /// <returns><c>true</c>, if user profile was updated, <c>false</c> otherwise.</returns>
        /// <param name="userProfile">User profile.</param>
        /// <param name="userID">User identifier.</param>
        public bool UpdateUserProfile(UserProfile userProfile, int userID)
        {
            bool updateUserProfile = false;
            if(userID > 0)
            {
                if(userProfile.BirthDate.Year < 1900)
                {
                    userProfile.BirthDate = _userFactory.GetUserBirthDate(userID);
                }
                updateUserProfile = _userFactory.UpdateUserProfile(userProfile, userID);
            }
            return updateUserProfile;
        }

        /// <summary>
        /// Gets the user birth date.
        /// </summary>
        /// <returns>The user birth date.</returns>
        /// <param name="userID">User identifier.</param>
        public DateTime GetUserBirthDate(int userID)
        {
            DateTime birthDate = new DateTime();
            if(userID > 0)
            {
                birthDate = _userFactory.GetUserBirthDate(userID);   
            }
            return birthDate;
        }

        /// <summary>
        /// Creates new LoginKey for token refresh
        /// </summary>
        /// <returns>The new login key.</returns>
        /// <param name="loginKey">Login key.</param>
        public User CreateNewLoginKey(string loginKey)
        {
            var user = new User();
            user.UserID = _userFactory.GetUserIDByLoginkey(loginKey);
            if(user.UserID > 0)
            {
				user.ExpirationMinutes = DateTime.UtcNow.AddMinutes(Convert.ToInt32(GetJsonValue("Token:ExpirationMinutes")));
                // var _expirationMinutes = DateTime.UtcNow.AddMinutes(1);
				var newGeneratedLoginKey = _userFactory.UpdateUserLogin(user.UserID, user.ExpirationMinutes);
                if(newGeneratedLoginKey.Length == 36)
                {
					
                    user.LoginKey = newGeneratedLoginKey;
                    user = _userFactory.GetLoginCredentials(user.UserID);
                }
            }
            return user;
        }

        /// <summary>
        /// Checks the token is blacklisted.
        /// </summary>
        /// <returns><c>true</c>, if token is blacklisted was checked, <c>false</c> otherwise.</returns>
        /// <param name="token">Token.</param>
        public bool CheckTokenIsNotBlacklisted(string token)
        {
            bool result = false;
            if (token != null)
            {
                var isTokenBlacklisted = _userFactory.CheckUserTokenIsBlacklisted(token);
                if(!isTokenBlacklisted)
                {
                    result = true;
                }
            }
            return result;
        }

        /// <summary>
        /// Inserts the token to blacklist.
        /// </summary>
        /// <returns><c>true</c>, if token to blacklist was inserted, <c>false</c> otherwise.</returns>
        /// <param name="tokenBlacklist">Token blacklist.</param>
        public bool InsertTokenToBlacklist(UserTokenBlacklist tokenBlacklist)
        {
            bool result = false;
            if(tokenBlacklist != null)
            {
                result = _userFactory.InsertTokenBlacklist(tokenBlacklist);
            }
            return result;
        }
    }
}
