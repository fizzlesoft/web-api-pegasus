﻿using System;
using System.Collections.Generic;
using System.Linq;
using Pegasus.DAL.Factories;
using Pegasus.DAL.Models;

namespace Pegasus.DAL.Repositories
{
    public class ContractRepository
    {
        ContractFactory _contractFactory = new ContractFactory();

        public IList<Contract> GetContractDetails(int? regionID)
        {
            var contract = new List<Contract>();
            contract = _contractFactory.GetContractDetails(regionID).ToList();
            return contract;
        }
    }
}
