﻿using System;
namespace Pegasus.DAL.Interfaces
{
    public interface IContractor
    {
		string CreateContractorLink(int userID, int contractID);
    }
}
