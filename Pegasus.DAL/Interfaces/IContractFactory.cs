﻿using System;
using System.Collections.Generic;
using Pegasus.DAL.Models;

namespace Pegasus.DAL.Interfaces
{
    public interface IContractFactory
    {
        IList<Contract> GetContractDetails(int? regionID);
    }
}
