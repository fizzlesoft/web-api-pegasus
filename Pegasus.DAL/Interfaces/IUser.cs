﻿using System;
using Pegasus.DAL.Models;
namespace Pegasus.DAL.Interfaces
{
    public interface IUser
    {
        int CheckUsernameAndPassword(string username, string password);
        User GetLoginCredentials(int userID);
        bool CheckUserExist(string username, string email, string contactNumber);
        bool UpdateNewPassword(string newPassword, int userID);
        UserDetails GetUserDetails(int userID);
        bool CreateUserProfile(UserProfile userProfile, int userID);
        UserProfile GetUserProfile(int userID);
        bool UpdateUserProfile(UserProfile userProfile, int userID);
        bool IsUserBanned(int userID);
        bool IsActiveLogin(int userID);
        bool InsertActiveLogin(UserLogin userLogin);
        LoginAttempt FailLoginAttempt(string username);
        bool IsTokenValid(string loginKey);
    }
}
